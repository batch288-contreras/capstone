const express = require('express');
const productsControllers = require('../controllers/productsControllers.js');
const router = express.Router();
const auth = require("../auth.js")

// non verif routes
router.get('/allProducts', auth.verify, productsControllers.getAllProducts);

router.get('/',  productsControllers.getAvailableProducts);
router.get('/:productId',productsControllers.getProduct);


// verif routes
router.post('/addProducts', auth.verify, productsControllers.addProducts);
router.patch('/:productId', auth.verify, productsControllers.updateProducts);
router.patch("/:productId/archiveProducts", auth.verify, productsControllers.archiveProducts);
router.patch("/:productId/activateProducts", auth.verify, productsControllers.activateProducts);

module.exports = router;
