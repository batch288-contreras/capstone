const Users = require("../models/Users.js");
const bcrypt = require("bcrypt");
const auth = require('../auth.js');
const Products = require('../models/Products.js');

// Register User
module.exports.registerUser = (request, response) => {
  Users.findOne({ email: request.body.email })
    .then(result => {
      if (result) {
        return response.send(false);
      } else {
        let newUser = new Users({
          isAdmin: request.body.isAdmin,
          email: request.body.email,
          password: bcrypt.hashSync(request.body.password, 10),
          firstName: request.body.firstName,
          lastName: request.body.lastName,
          address: request.body.address,
          mobileNo: request.body.mobileNo,
          createdOn: request.body.createdOn
        });
        newUser.save()
          .then(saved => response.send(result))
          .catch(error => response.send(false));console.log(error)
      }
    })
    .catch(error => response.send(error));

};


// User login
module.exports.loginUser = (request, response) => {
Users.findOne({email : request.body.email})
  .then(result => {

    if(!result){
      return response.send(false)
    }else{
      //the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve form the find method. returns true or false depending the result of the comparison.
      const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

      if(isPasswordCorrect){
        return response.send({auth: auth.createAccessToken(result)}
        )
      }else{
        return response.send(false)
      }


    }

  })
  .catch(error => response.send(error));
}

// get user ordered items
module.exports.getOrderedItems = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  if (userData.isAdmin) {
     return response.send(`Unable to retrieve ordered items as admin`);
  } else {
       Users.findById(userData.id) 
      .select("orderedProduct")
      .populate("orderedProduct.productId", "name")
      .then((result) => response.send(result.orderedProduct))
      .catch((error) => response.send(error));
  }
};

// gets specific user data admin only
module.exports.getProfile = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin) {
    Users.findById(request.params.id)
      .then(result => {
        result.password = "*************";
        return response.send(result);
      })
      .catch(error => {
        return response.send(`Error: ${error.message}`);
      });
  } else {
    return response.send(`You don't have the authorization to access this route.`);
  }
};


// gets all non admin user admin only

module.exports.getAllProfile = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin) {
    Users.find({})
      .then(results => {
        const profiles = results.map(result => {
          result.password = "*************";

          return result;
        });
        return response.send(profiles);
      })
      .catch(error => {
        return response.send(error);
      });
  } else {
    return response.send(`You don't have the authorization to access this route.`);
  }
};

// for non admin user ordering of products
//productId
module.exports.orderProducts = (request, response) => {
  const productId = request.body.id;
  const orderQuantity = request.body.quantity;
  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin) {
    return response.send("Admin cannot order products");
  } else {
    let isUserUpdated = Users.findOne({ _id: userData.id })
      .then((result) => {
        result.orderedProduct.push({
          productId: productId,
          quantity: orderQuantity,
          totalAmount: 0,
          orderedDate: new Date(),
        });

        result.save()
          .then((savedUser) => true)
          .catch((error) => {
            return false;
          });
      })
      .catch((error) => {
        return false;
      });

    let isProductUpdated = Products.findOne({ _id: productId })
      .then((result) => {
                const productPrice = result.price;
        result.userOrders.push({
          userId: userData.id,
          quantity: orderQuantity,
        });

        result.save()
          .then((savedProduct) => true)
          .catch((error) => {
            return false;
          });
      })
      .catch((error) => {
        return false;
      });

    if (isUserUpdated && isProductUpdated) {
      Products.findOne({ _id: productId })
        .then((result) => {
          const productPrice = result.price;
          const totalAmount = productPrice * orderQuantity;

          Users.findOneAndUpdate(
            { _id: userData.id, "orderedProduct.productId": productId },
            { $set: { "orderedProduct.$.quantity": orderQuantity, "orderedProduct.$.totalAmount": totalAmount } }
          )
            .then((updatedUser) => {
              return response.send(true);
            })
        })
        .catch((error) => {
          return response.send(false);
        });
    } else {
      return response.send(false);
    }
  }
};



// user payment for ordered products 

 module.exports.orderPayments = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const orderId = request.body.orderId;
  const paymentAmount = request.body.paymentAmount;

  Users.findOne({ _id: userData.id, "orderedProduct._id": orderId })
    .then((user) => {
      const orderedProduct = user.orderedProduct.find((product) => product._id.toString() === orderId);

      if (!orderedProduct) {
        return response.send('Invalid order ID');
      }

      const totalAmount = orderedProduct.totalAmount;
      const change = paymentAmount - totalAmount;

      if (change < 0) {
        return response.send('Insufficient payment amount');
      }

      orderedProduct.orderPayment = paymentAmount;
      orderedProduct.totalchange = change;
      orderedProduct.isPayed = true;

      user.save()
        .then((updatedUser) => {
          response.send(true); // Sending a boolean true as a response
        })
        .catch((error) => {
          response.send(false);
        });
    })
    .catch((error) => {
      response.send(false);
    });
};




// get the current log in user profile
module.exports.getCurrentUser = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  Users.findOne({_id: userData.id})
  .then(data=> response.send(data))
}


// admin get all orders
module.exports.getAllOrders = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  if (userData.isAdmin) {
    Users.find({isAdmin:false}, { email: 0, address: 0, isAdmin: 0, userData: 0, password: 0, mobileNo: 0, __v: 0 })
      .then(result => {
        return response.send(result);
      })
      .catch(error => {
        return response.send(error);
      });
  } else {
    return response.send(`You don't have the authorization to access this route`);
  }
};
