import './App.css';
import {useState, useEffect} from 'react';
import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Products from './pages/Products.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js'
import ProductView from './pages/ProductView.js'
import {UserProvider} from './UserContext.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import AllProducts from './pages/AllProducts.js'
import CreateProducts from './pages/CreateProduct.js'
import EditProduct from './pages/EditProduct.js'
import ArchiveProduct from './pages/ArchiveProduct.js'
import Cart from './pages/Cart.js'
import Users from './pages/Users.js'

function App() {

  let userDetails = {};

  useEffect(()=>{
    if(localStorage.getItem('token') === null){
      userDetails = {
        id: null,
        isAdmin: null
      }
    }else{
        fetch(`${process.env.REACT_APP_API_URL}/users//myProfile`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          userDetails = {
              id : data._id,
              isAdmin: data.isAdmin
          };

      })
    }
  }, [])
  
  
  const [user, setUser] = useState(userDetails);

  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(()=> {
    console.log(user);
    console.log(localStorage.getItem('token'));
  }, [user])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
        <BrowserRouter>
          <AppNavBar />
            <Routes>
              <Route path = '/' element = {<Home/>} />
              <Route path = '/products' element = {<Products/>} />
              <Route path = '/register' element = {<Register/>} />
              <Route path = '/login' element = {<Login/>} />
              <Route path = '/logout' element = {<Logout/>} />
              <Route path = '*' element = {<PageNotFound/>} />
              <Route path = '/products/:productId' element = {<ProductView/>} />
              <Route path = '/allProducts' element = {<AllProducts/>} />
              <Route path = '/createProduct' element = {<CreateProducts/>} />
              <Route path = '/editProduct/:productId' element = {<EditProduct/>} />
              <Route path = '/archiveProduct/:productId' element = {<ArchiveProduct/>} />
              <Route path = '/cart/' element = {<Cart/>} />
              <Route path = '/users/' element = {<Users/>} />
            </Routes>
        </BrowserRouter>
    </UserProvider>
    

  );
}

export default App;
