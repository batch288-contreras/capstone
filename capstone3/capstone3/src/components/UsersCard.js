import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function ProductCard(props) {
  const { user } = useContext(UserContext);
  const { _id, email, firstName, lastName, address, mobileNo } = props.userProp;

  return (
    <Col xs={12} md={4} className="mb-3">
      <Card>
        <Card.Body>
          <Card.Title className="mb-3">User ID: {_id}</Card.Title>
          <Card.Subtitle>Email:</Card.Subtitle>
          <Card.Text>{email}</Card.Text>

          <Card.Subtitle>First Name:</Card.Subtitle>
          <Card.Text>{firstName}</Card.Text>
              
          <Card.Subtitle>Last Name:</Card.Subtitle>
          <Card.Text> {lastName}</Card.Text>

          <Card.Subtitle>Address:</Card.Subtitle>
          <Card.Text>{address}</Card.Text>

          <Card.Subtitle>Mobile No:</Card.Subtitle>
          <Card.Text>{mobileNo}</Card.Text>



        </Card.Body>
      </Card>
    </Col>
  );
}
