import { Button, Container, Row, Col } from 'react-bootstrap';
import Accordion from 'react-bootstrap/Accordion';
import { Link } from 'react-router-dom';
import React, { useContext } from 'react';
import UserContext from '../UserContext';

export default function Banner() {
  const { user } = useContext(UserContext);
  const isAdmin = user && user.isAdmin;

  return (
    <Container>
      <Row>
        {isAdmin ? (
          <>
            <h1 className="text-md-end text-sm-center mt-3">Admin Dashboard</h1>
            <Col className="col-12 col-md-3">
              <Accordion>
                <Accordion.Item eventKey="0">
                  <Accordion.Header>Products</Accordion.Header>
                  <Accordion.Body>
                    <Link to="/allProducts" className="link-style">
                      View All Products
                    </Link>
                  </Accordion.Body>
                  <Accordion.Body>
                    <Link to="/createProduct" className="link-style">
                      Add Product
                    </Link>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                  <Accordion.Header>Users</Accordion.Header>
                  <Accordion.Body>
                    <Link to="/users" className="link-style">
                      View All Users
                    </Link>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            </Col>

            <Col className="col-8 d-none d-md-block">
              <img
                className="d-block w-100"
                src="admin.png"
                alt="Admin Dashboard"
              />
            </Col>
          </>
        ) : (
          <Col className="col-12 mt-3 text-center">
            <h1>GGWP Online Shop Centre</h1>
            <p>"Every Gamer wants. Every Gamer needs!"</p>
            <Button variant="dark" as={Link} to={'/products'} className="link-style">
              {'Shop Now!'}
            </Button>
          </Col>
        )}
      </Row>
    </Container>
  );
}
