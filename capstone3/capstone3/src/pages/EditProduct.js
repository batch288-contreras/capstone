import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import Swal2 from 'sweetalert2';

export default function EditProduct() {
 	const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [productCategory, setProductCategory] = useState('');
  const [price, setPrice] = useState('');
  const [productStocks, setProductStocks] = useState('');
  const navigate = useNavigate();
  const { productId } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(response => response.json())
      .then(data => {
        if (data) {
          setProductName(data.productName);
          setDescription(data.description);
          setProductCategory(data.productCategory)
          setPrice(data.price);
          setProductStocks(data.productStocks);
        }
      })
      .catch(error => {
        console.log('Error:', error);
      });
  }, [productId]);

  function updateProduct(event) {
    event.preventDefault();

    const requestBody = {
      productName: productName,
      description: description,
      productCategory: productCategory,
      price: parseFloat(price),
      productStocks: parseInt(productStocks),
      isActive: true,
    };

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: 'PATCH',
      headers: {
       'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(requestBody),
    })
      .then(response => response.json())
      .then(data => {
        if (!data) {
          Swal2.fire({
            title: 'Error',
            icon: 'error',
            text: 'Please try again!',
          });
        } else {
          Swal2.fire({
            title: 'Product Update',
            icon: 'success',
            text: 'Product has been updated!',
          });
        }
      })
      .catch(error => {
        console.log('Error:', error);
      });
  }

  return (

    <Container className="mt-5">
      <Row>
        <Col className="col-12 col-md-6 mx-auto">
          <h1 className="text-center mb-3">Update Product</h1>
          <Form onSubmit={event => updateProduct(event)}>
            <Form.Group className="mb-3" controlId="formProductName">
              <Form.Label>Product Name:</Form.Label>
              <Form.Control
                type="text"
                value={productName}
                onChange={event => setProductName(event.target.value)}
                placeholder="Enter Product Name"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formDescription">
              <Form.Label>Description:</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={event => setDescription(event.target.value)}
                placeholder="Enter Description"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formProductCategory">
              <Form.Label>Product Category:</Form.Label>
              <Form.Control
                type="text"
                value={productCategory}
                onChange={event => setProductCategory(event.target.value)}
                placeholder="Enter Product Category"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formPrice">
              <Form.Label>Price:</Form.Label>
              <Form.Control
                type="text"
                value={price}
                onChange={event => setPrice(event.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formProductStocks">
              <Form.Label>Stocks:</Form.Label>
              <Form.Control
                type="text"
                value={productStocks}
                onChange={event => setProductStocks(event.target.value)}
              />
            </Form.Group>



            <Row>
              <Col className="text-center">
                <Button variant="dark" type="submit">
                  Submit
                </Button>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
