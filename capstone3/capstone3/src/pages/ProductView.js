import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function ProductView() {
  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const navigate = useNavigate();
  const [quantity, setQuantity] = useState(1);
  const { user } = useContext(UserContext);
  const { productId } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(result => result.json())
      .then(data => {
        setProductName(data.productName);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, []);

  const order = (productID) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/orderProducts`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: productID,
        quantity: quantity
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data) {
          Swal2.fire({
            title: 'Success',
            icon: 'success',
            text: "The product has been added to cart!"
          });
          navigate('/products');
        } else {
          Swal2.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again!'
          });
        }
      });
  };

  return (
    <Container className='mt-4'>
      <Row>
        <Col className='col-12 mt-3 ms-auto mt-sm-auto'>
          <Card>
            <Card.Body>
              <Card.Title>{productName}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Text>Price: {price}</Card.Text>

              {!user.isAdmin && (
                <>
                  <Card.Text>
                    Quantity: <input type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
                  </Card.Text>
                  <Button variant="dark" onClick={() => order(productId)}>Add to Cart</Button>
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
        <Col className='col 6 mt-3'></Col>
      </Row>
    </Container>
  );
}
