import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import Swal2 from 'sweetalert2';

export default function ArchiveProduct() {
 	const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [productCategory, setProductCategory] = useState('');
  const [price, setPrice] = useState('');
  const [productStocks, setProductStocks] = useState('');
  const [isActive, setisActive] = useState('');
  const navigate = useNavigate();
  const { productId } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(response => response.json())
      .then(data => {
        if (data) {
          setProductName(data.productName);
          setDescription(data.description);
          setProductCategory(data.productCategory)
          setPrice(data.price);
          setProductStocks(data.productStocks);
          setisActive(data.isActive);
        }
      })
      .catch(error => {
        console.log('Error:', error);
      });
  }, [productId]);

  function archiveProduct(event) {
  event.preventDefault();

  const requestBody = {
    isActive: !isActive
  };

  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archiveProducts`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
    body: JSON.stringify(requestBody),
  })
    .then(response => response.json())
    .then(data => {
      if (!data) {
        Swal2.fire({
          title: 'Error',
          icon: 'error',
          text: 'Please try again!',
        });
      } else {
        Swal2.fire({
          title: 'Product Update',
          icon: 'success',
          text: 'Product has been updated!',
        });
        setisActive(!isActive); 
      }
    })
    .catch(error => {
    });
}







  return (
   <Container>
      <Row>
        <Col className="mt-3 mx-auto">
          <Card>
            <Card.Body>
              <Card.Title className="mb-3">{productName}</Card.Title>
              <Card.Subtitle>Category:</Card.Subtitle>
              <Card.Text>{productCategory}</Card.Text>

              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              <Card.Subtitle>Stocks:</Card.Subtitle>
              <Card.Text>{productStocks}</Card.Text>

              <Card.Subtitle>Available:</Card.Subtitle>
              <Card.Text>{isActive ? 'Yes' : 'No'}</Card.Text>

              
                <Button variant='dark'onClick={archiveProduct} >
                  Archive / Activate
                </Button>
            
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}