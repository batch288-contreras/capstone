import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';


export default function CreateProduct() {
  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [productCategory, setProductCategory] = useState('');
  const [price, setPrice] = useState('');
  const [productStocks, setProductStocks] = useState('');
  const [isDisabled, setIsDisabled] = useState(true);

  const navigate = useNavigate();

  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
     if (
      productName !== '' &&
      description !== '' &&
      productCategory !== '' &&
      price !== '' &&
      productStocks !== ''
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [productName, description, productCategory, price, productStocks]);

  function registerProduct(event) {
    event.preventDefault();

    const requestBody = {
      productName: productName,
      description: description,
      productCategory: productCategory,
      price: parseFloat(price),
      productStocks: parseInt(productStocks),
      isActive: true,


    };

    fetch(`${process.env.REACT_APP_API_URL}/products/addProducts`, {
      method: 'POST',
      headers: {
         'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(requestBody),
    })
      .then(response => response.json())
      .then(data => {
        if (!data) {
          Swal2.fire({
            title: 'Error',
            icon: 'error',
            text: 'Please try again!',
          });console.log(data)
        } else {
          Swal2.fire({
            title: 'Success',
            icon: 'success',
            text: 'Product has successfully been registered!',
          });
            setProductName('');
            setDescription(''); 
            setProductCategory('');
            setPrice(''); 
            setProductStocks('');
    }
      });
  }

  return (
    <Container className="mt-5">
      <Row>
        <Col className="col-12 col-md-6 mx-auto">
          <h1 className="text-center mb-3">Add Product</h1>
          <Form onSubmit={event => registerProduct(event)}>
            <Form.Group className="mb-3" controlId="formProductName">
              <Form.Label>Product Name:</Form.Label>
              <Form.Control
                type="text"
                value={productName}
                onChange={event => setProductName(event.target.value)}
                placeholder="Enter Product Name"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formDescription">
              <Form.Label>Description:</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={event => setDescription(event.target.value)}
                placeholder="Enter Product Description"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formProductCategory">
              <Form.Label>Product Category:</Form.Label>
              <Form.Control
                type="text"
                value={productCategory}
                onChange={event => setProductCategory(event.target.value)}
                placeholder="Enter Product Category"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formPrice">
              <Form.Label>Price:</Form.Label>
              <Form.Control
                type="text"
                value={price}
                onChange={event => setPrice(event.target.value)}
                placeholder="Enter Product Price"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formProductStocks">
              <Form.Label>Stocks:</Form.Label>
              <Form.Control
                type="text"
                value={productStocks}
                onChange={event => setProductStocks(event.target.value)}
                placeholder="Enter Product Stocks"
              />
            </Form.Group>

            <Row>
              <Col className="text-center">
                <Button variant="dark" type="submit" disabled={isDisabled}>
                  Submit
                </Button>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
