import UsersCard from '../components/UsersCard.js';
import { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';



  export default function AllUsers() {
const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(result => result.json())
      .then(data => {
        setUsers(data);
      });
  }, []);

  return (
    <Container>
      <h1 className="text-center mt-3">Users</h1>
      <Row>
        {users.map(user => (
          <UsersCard key={user._id} userProp={user} />
        ))}
      </Row>
    </Container>
  );
}
