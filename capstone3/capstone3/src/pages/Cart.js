import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function CartView() {
  const [cartItems, setCartItems] = useState([]);
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/orderedItems`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        if (Array.isArray(data)) {
          setCartItems(data);
        } else {
          console.log('Invalid response data:', data);
        }
      })
      .catch(error => {
        console.log('Error fetching ordered items:', error);
      });
  }, []);

  function handlePayment(itemId, payment) {
    const requestBody = {
      itemId: itemId,
      orderPayment: payment
    };

    fetch(`${process.env.REACT_APP_API_URL}/users/orderPayments`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(requestBody),
    })
      .then(response => response.json())
      .then(data => {
        if (data) {
          Swal2.fire({
            title: 'Product Update',
            icon: 'success',
            text: 'Product has been updated!',
          });
        } else {
          Swal2.fire({
            title: 'Error',
            icon: 'error',
            text: 'Please try again!',
          });
        }
      })
      .catch(error => {
        console.log('Error:', error);
      });
  }

  return (
  <Container className="mt-3">
    {cartItems.map(item => (
      <Row key={item._id}>
        <Col className='col-12 mb-3 ms-auto mt-sm-auto'>
          <Card>
            <Card.Body>
              <Card.Title>Reference Number: {item._id}</Card.Title>
              <Card.Subtitle className='mt-3'>Product ID: {item.productId}</Card.Subtitle>
              <Card.Text className='mt-3 mb-2'>Quantity: {item.quantity}</Card.Text>
              <Card.Text className='mb-2'>Price: {item.totalAmount / item.quantity}</Card.Text>
              <Card.Text className='mb-2'>Total Amount: {item.totalAmount}</Card.Text>
              <Card.Text className='mb-2'>Ordered Date: {new Date(item.orderedDate).toLocaleDateString('en-GB')}</Card.Text>
              <Card.Text className='mb-2'>Payed: {item.isPayed ? 'Yes' : 'No'}</Card.Text>
              {!item.isPayed && (
                <Card.Text>
                  Enter Payment: <input
                    type="text"
                    value={item.orderPayment}
                    onChange={e => handlePayment(item._id, e.target.value)}
                  />
                </Card.Text>
              )}
              {item.isPayed && (
                <Card.Text className='mb-2'>Order Payment: {item.orderPayment}</Card.Text>
              )}
              <Button variant='dark' onClick={() => handlePayment(item._id, item.orderPayment)}>
                Order
              </Button>            </Card.Body>
          </Card>
        </Col>
      </Row>
    ))}
  </Container>
);
}
